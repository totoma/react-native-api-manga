import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import CreateScreen from './screens/CreateScreen';
import HomeScreen from './screens/HomeScreen';
import List from './components/List';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import ListScreen from './screens/ListScreen';
import DetailScreen from './screens/DetailScreen';

const Stack = createNativeStackNavigator()

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen}></Stack.Screen>
        <Stack.Screen name="Create" component={CreateScreen} ></Stack.Screen>
        <Stack.Screen name="ListOfManga" component={ListScreen} ></Stack.Screen>
        <Stack.Screen name="DetailManga" component={DetailScreen} ></Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  )
}