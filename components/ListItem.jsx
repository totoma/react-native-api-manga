import { View, Text, Image, StyleSheet, SafeAreaView, TouchableHighlight,  } from "react-native";
import ButtonDelete from './Buttons/ButtonDelete';
import {useNavigation } from '@react-navigation/native'

export default function ListItem(props){

    const navigation = useNavigation()

    const styles = StyleSheet.create({
        tinyLogo: {
            width: 300,
            height: 300,
            resizeMode: 'contain'
          }
    })
    return(
        <SafeAreaView>
            <TouchableHighlight onPress={ () => navigation.navigate("DetailManga", {data: props.name._id})}>
            <Image style={styles.tinyLogo} source={props.name.url}>
            </Image>
            </TouchableHighlight>
            <Text>
                {props.name.name + "-"}
                {props.name.author}
            </Text>
            <ButtonDelete data={props.name._id}></ButtonDelete>
        </SafeAreaView>
    )
}