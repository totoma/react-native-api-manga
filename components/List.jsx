import React from 'react';
import { SafeAreaView, FlatList } from 'react-native';
import ListItem from './ListItem'

export default function List(props){
    const renderItem = ({item}) => (
        <ListItem name={item}></ListItem>
    ) 
    return (
        <SafeAreaView>
          <FlatList
            data={props.data}
            renderItem= {renderItem}
            keyExtractor={item => item._id}
          />
        </SafeAreaView>
      );
}