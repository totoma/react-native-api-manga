import axios from 'axios';
import { useEffect } from 'react';
import { Button } from 'react-native';

export default function deleteButton(props) {

        const deleteManga = () => {
          axios.delete('http://localhost:8000/manga/deleteMangas/' + props.data)
      } 
      
      return(
        <Button onPress={deleteManga} title="delete"></Button>
      )
}
