import axios from "axios";
import { useState } from "react";
import { View, Button, TextInput } from "react-native";

export default function CreateScreen({navigation}){

    // défini un state avec une méthode setName, les données du state sont accessibles grâce à la variable name
    const [name, setName] = useState('');
    const [author, setAuthor] = useState('');
    const [url, setUrl] = useState('');
    const [idTypeManga, setIdTypeManga] = useState('');
    
    const createManga = () => {
        axios.post('http://localhost:8000/manga/createManga', {name: name,author: author,url: url,id_type_manga: idTypeManga})
        .then(res => console.log(res))
        .catch(err => console.log(err))
    }

    return(
        <View style={{
            flex:1,
            alignItems:"center",
            justifyContent: "center"
        }}>
            <form>
                <label>
                    Name:
                </label>
                    <TextInput type="text" name="name" onChangeText={(text) => setName(text) } ></TextInput>
                <label>
                    Author:
                </label>
                <TextInput type="text" name="author" onChangeText={(text) => setAuthor(text) }></TextInput>
                <label>
                    url:
                </label>
                <TextInput type="text" name="url" onChangeText={(text) => setUrl(text) }></TextInput>
                <label>
                idTypeManga :
                </label>
                    <TextInput type="text" name="idTypeManga" onChangeText={(text) => setIdTypeManga(text) } ></TextInput>
                <Button type="submit" title="Submit" onPress={ () => createManga()} />
            </form>

            <Button title="ListOfManga" onPress={ () => navigation.navigate("ListOfManga")}></Button>
        </View>
    )
}