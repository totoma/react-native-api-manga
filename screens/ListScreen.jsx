import axios from 'axios';
import { StyleSheet, Text, View } from 'react-native';
import React, {useState, useEffect} from 'react';
import List from '../components/List';

export default function ListScreen({route}) {

  // const {objectId, nameObject} = route.params
  const [mangas, setMangas] = useState([])

  useEffect(() => {
      const allMangas = async () => {
        const request = (await axios.get('http://localhost:8000/manga/getAllMangas')).data.mangas
        setMangas(request)
    } 
    allMangas()
  }, [])
  
  return (
    <View style={styles.container}>
      <List data={mangas}></List>
    </View>
  );
  }

  const styles = StyleSheet.create({
    container: {
     
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });