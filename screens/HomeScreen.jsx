import { View, Button } from "react-native"

export default function HomeScreen({navigation}){
    
    return(
        <View style={{
            flex:1,
            alignItems:"center",
            justifyContent: "center"
        }}>
            <Button title="Create" onPress={ () => navigation.navigate("Create", {
                objectId: 1,
                nameObject: "test"
            })}></Button>
            <Button title="list of manga" onPress={ () => navigation.navigate("ListOfManga", {
                objectId: 1,
                nameObject: "test"
            })}></Button>

        </View>
    )
}