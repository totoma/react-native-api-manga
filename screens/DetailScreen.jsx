import { useEffect, useState } from "react";
import axios from "axios";
import { View, Text } from "react-native";

export default function DetailScreen({route}){

    const [dataState, setDataState] = useState([]);
    
    const {data} = route.params

    useEffect(() => {
    
    const detailManga = async () => {
        await axios.get('http://localhost:8000/manga/getManga/' + data)
        .then(res => setDataState(res.data.mangas))
        .catch(err => console.log(err))
    }
    detailManga()
},[])

    return(
        <View>
            console.log({dataState.name});
            <Text>{dataState.url}</Text>
            <Text>{dataState.name}</Text>
            <Text>{dataState.author}</Text>
        </View>
    )
}